from django.conf.urls import patterns, include, url
from django.contrib import admin
from accounts.forms import ProfileForm
from defaultapp.feeds import LatestArticles

from samalinews_com import settings

#import filebrowser.sites

admin.autodiscover()

feeds = { 'latest' : LatestArticles }

urlpatterns = patterns('',
    url(r'^$', 'defaultapp.views.home', name='home'),
    url(r'^article/(?P<sl>[-\w]+)/$', 'defaultapp.views.show_article', name='article'),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),

    url(r'^tinymce/', include('tinymce.urls')),

    (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),

    url(r'^categories/([-\w]+)/$', 'defaultapp.views.show_category', name='category'),
    url(r'^tags/([-\w]+)/$', 'defaultapp.views.tag', name='tag'),
    url(r'^latest/', 'defaultapp.views.show_latest'),
    url(r'^popular/', 'defaultapp.views.show_popular'),

    url(r'^accounts/', include('registration.backends.default.urls')),
    url(r'^profiles/edit/$', 'profiles.views.edit_profile', {'form_class': ProfileForm}, 'profiles_edit_profile'),
    url(r'^profiles/', include('profiles.urls')),

    url(r'^search/', 'defaultapp.views.search_article', name="search_article"),
    
    url(r'^comments_app/add_comment', 'comments_app.views.add_comment'),
    
    url(r'^rss/(?P<url>.*)/$', LatestArticles()),

)
