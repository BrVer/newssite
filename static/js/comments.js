(function ($) {
    $.fn.bindToggleFormHandler = function () {
        this.each(function () {
            $(this).click(function () {
                cf = $(this).parent().parent().children('.comment-form')
                if(cf.length > 0){
                    $(cf).toggle();
                } else {
                    $(this).parent().parent().append($('.comment-form'));
                    $('.comment-form').show();
                }
                $('#id_parent_id').val($(this).attr('id').substring(1));
                $('#id_text').focus();
            });
        });
    };

    $.fn.bindPostCommentHandler = function () {
        $(this).submit(function (event) {
            event.preventDefault();
            parent = $(this).parent().parent();
            ourul = $(parent).children('ul.commentsul').first()
            $.ajax({
                url: $(this).attr('action'),
                dataType:'json',
                data: $(this).serialize(),
                type: "POST",
                success: function (data) {
                    $('.comment-form').hide();
                    $('#id_text').val('');
                    insert_html = '<li class ="li_comment" id="' + data.id + '"><div class="well well-small"><div class="row-fluid"><div class="span4 user">'
                        + '<a href ="/profiles/' + data.author + '">' + data.author + '</a></div><div class="date">' + data.added + '</div></div>'
                        + '<div class="row-fluid message"><p>' + data.text + '</p><button class="btn reply-btn pull-right" id="b' + data.id + '">Reply</button></div></div></li>'
                    if(ourul.length > 0) {
                        $(ourul).append(insert_html);
                    } else {
                        $(parent).append('<ul class="unstyled commentsul">'+insert_html+'</ul>');
                    }
                    $('ul#comments').append($('#b').parent());
                    $('ul#comments').append($('.comment-form'))
                    $('#b' + data.id).bindToggleFormHandler();
                }
            });
        });
    };
})(jQuery);

$(document).ready(function () {
    $('.reply-btn').bindToggleFormHandler();
    $('.comment-form').bindPostCommentHandler();
});

