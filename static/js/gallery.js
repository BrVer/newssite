$(document).ready(function() {
  	
	$("#gallery").carouFredSel({
	    circular: false,
	    infinite: false,
	    auto    : false,
	   // items : {
	   // 			minimum : 3
	   // 		}  ,
	    prev    : {
        button  : "#gallery_prev",
	        key     : "left"
	    },
	    next    : {
	        button  : "#gallery_next",
	        key     : "right"
	    },
	    pagination  : "#gallery_pag"
	});
  	
});

