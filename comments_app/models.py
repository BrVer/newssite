from django.db import models
from defaultapp.models import *
from mptt.models import MPTTModel, TreeForeignKey
from django.contrib.auth.models import User

class MPTTComment(MPTTModel):
    """ Threaded comments for blog posts """
    article = models.ForeignKey(Article, related_name="comments")
    author = models.ForeignKey(User)
    text = models.TextField(max_length=500)
    added  = models.DateTimeField(default= datetime.datetime.now)
    # a link to comment that is being replied, if one exists
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children')

    def __unicode__(self):
        return u'article %s - author %s - text %s' % (self.article.title, self.author.username, self.text)

    class MPTTMeta:
        # comments on one level will be ordered by date of creation
        order_insertion_by=['added']
