from django import template
from comments_app.forms import MPTTCommentForm

register = template.Library()

@register.inclusion_tag("comments_app/form.html")
def comment_form(user_id, article_id):
    form = MPTTCommentForm(initial={'author_id': user_id, 'article_id': article_id})
    return {'form': form}


