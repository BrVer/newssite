from models import MPTTComment
from forms import MPTTCommentForm
from django.http import *
from defaultapp.models import Article
from django.utils.timesince import timesince
import django.utils.simplejson as json

def add_comment(request):
    if request.method == 'POST':        # if the form has been submitted
        form = MPTTCommentForm(request.POST)
        if form.is_valid():
            comment = form.save(request.user)
            if request.is_ajax():
                data = json.dumps({'added':comment.added.strftime("%b %d %Y %H:%M:%S"), 'author':comment.author.username, 'id':comment.id, 'text':comment.text})
                return HttpResponse(data, content_type="application/json;charset=utf-8")

    return HttpResponseBadRequest()
