from django import forms
from models import MPTTComment


class MPTTCommentForm(forms.Form):
    parent_id = forms.IntegerField(required=False, widget=forms.HiddenInput)
    article_id = forms.IntegerField(widget=forms.HiddenInput)
    text = forms.CharField(max_length=500, widget=forms.Textarea)

    def save(self, user):
        comment = MPTTComment(
            author_id = user.id,
            article_id = self.cleaned_data['article_id'],
            text=self.cleaned_data['text']
        )
        if self.cleaned_data['parent_id'] != None:
            comment.parent = MPTTComment.objects.get(id=self.cleaned_data['parent_id'])
        comment.save()
        return comment

    def __unicode__(self):
        return u'article %s - author %s - text %s' % (self.article.title, self.author.username, self.text)

    class MPTTMeta:
        # comments on one level will be ordered by date of creation
        order_insertion_by=['added']