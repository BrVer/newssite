from django.contrib import admin
from mptt.admin import MPTTModelAdmin
from defaultapp.models import *
import feincms.admin.tree_editor as tree_editor
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from accounts.models import UserProfile
import tinymce.widgets
from django.contrib.contenttypes import generic


class MetaTagInline(generic.GenericTabularInline):
    model = Metatag
    extra = 0
    can_delete = False
    verbose_name_plural = 'META-tag'

# Define an inline admin descriptor for UserProfile model
# which acts a bit like a singleton
class UserProfileInline(admin.StackedInline):
    model = UserProfile
    can_delete = False
    verbose_name_plural = 'profile'

# Define a new User admin
class UserAdmin(UserAdmin):
    inlines = (UserProfileInline, )
    list_display = ('username','email','first_name', 'last_name',)
    search_fields = ('first_name', 'last_name')

class CategoryMPTTModelAdmin(MPTTModelAdmin, tree_editor.TreeEditor):
    prepopulated_fields = {'slug' : ('name', )} 
    mptt_level_indent = 30 
    list_display = ('name', 'active_toggle')
    active_toggle = tree_editor.ajax_editable_boolean('is_active', ('is_active'))
    inlines = (MetaTagInline, )

    def save_model(self, request, obj, form, change):
        super(CategoryMPTTModelAdmin, self).save_model(request, obj, form, change)
        if not(change):
            t = Metatag(content_object=obj)
            list = ['news']
            list+=obj.get_ancestors(True, True).values_list('name', flat = True)
            t.keywords = ', '.join(list)
            t.save()
    
class CommentMPTTModelAdmin(MPTTModelAdmin, tree_editor.TreeEditor):
    mptt_level_indent = 30
    list_filter = ('article', 'author')
    
class ArticleModelAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug' : ('title', )} 
    list_display = ('title', 'category', 'visits_count', 'active_toggle')
    search_fields = ('title',)
    list_filter = ('publication_date', 'visibility', 'tags')
    fields = ('category', 'title', 'preview', 'text', 'tags',
              'related_articles','publication_date', 'visibility', 'slug', 'is_important')
    raw_id_fields = ('related_articles',)
    active_toggle = tree_editor.ajax_editable_boolean('is_important', ('is_important'))
    filter_horizontal = ('tags', 'related_articles')
    inlines = (MetaTagInline, )
    
    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'text':
            return db_field.formfield(widget=tinymce.widgets.TinyMCE(
                attrs={'cols': 80, 'rows': 30},
            ))
        if db_field.name == 'preview':
            return db_field.formfield(widget=tinymce.widgets.TinyMCE(
                attrs={'cols': 80, 'rows': 30},
            ))
        return super(ArticleModelAdmin, self).formfield_for_dbfield(db_field, **kwargs)

    def save_model(self, request, obj, form, change):
        super(ArticleModelAdmin, self).save_model(request, obj, form, change)
        if not(change):
            t = Metatag.objects.create(content_object=obj)
            list = ['news']
            list+=obj.category.get_ancestors(True, True).values_list('name', flat = True)
            list+=form.cleaned_data['tags'].values_list('name', flat = True)
            t.description = obj.preview[:150]
            t.keywords = ', '.join(set(list))
            t.save()



class PictureModelAdmin(admin.ModelAdmin):
    list_display = ('name', 'get_thumbnail_html', 'image', 'article')
    fields = ('name', 'image', 'article',)
    raw_id_fields = ('article',)


admin.site.register(Category, CategoryMPTTModelAdmin)
admin.site.register(Tag)
admin.site.register(Article, ArticleModelAdmin)
admin.site.register(Picture, PictureModelAdmin)
admin.site.unregister(User)
admin.site.register(User, UserAdmin)
