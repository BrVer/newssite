'''
Created on 15.10.2012

@author: rika
'''
from BeautifulSoup import BeautifulSoup
from BeautifulSoup import BeautifulStoneSoup
from django.core.management.base import NoArgsCommand
import urllib
from defaultapp.models import Article, Category, Picture
from django.template.defaultfilters import title
import datetime
import re
from django.core.files import File
from _mysql_exceptions import Error
import tempfile


class Command(NoArgsCommand):
    help = "Steal news from BBC"

    def handle_noargs(self, **options):
        self.parse("Business", "http://feeds.bbci.co.uk/news/business/rss.xml")
        self.parse("Sport", "http://feeds.bbci.co.uk/sport/0/rss.xml?edition=uk")
        self.parse("Health", "http://feeds.bbci.co.uk/news/health/rss.xml")
        self.parse("Science", "http://feeds.bbci.co.uk/news/science_and_environment/rss.xml")
        
    def parse_rss(self, category, url):
        soup = BeautifulStoneSoup(urllib.urlopen(url))
        items = soup('item')
        links = []
        for i in items:
            title = i('title')[0].string
            if 'AUDIO' in title or 'VIDEO' in title:
                continue 
            if not Article.objects.filter(title=unicode(title)).exists():
                links.append({"title": title, "link": i('link')[0].string})
        return links[:5]
    
    def parse_html(self, link):
        
        def extract_tag(tag):
            if tag:
                tag.extract()
                
        def get_text(soup):
            tags = soup.find('div', attrs={'class' : 'story-body'})
            extract_tag(tags.find('span', attrs={'class' : 'story-date'}))
            extract_tag(tags.find('p', attrs={'class' : 'introduction'}))
            [t.extract() for t in tags.findAll('a')]
            extract_tag(tags.img)
            extract_tag(tags.find('div', attrs={'class' : 'warning'}))
            extract_tag(tags.find('div', attrs={'class' : 'aside'}))
            [t.extract() for t in tags.findAll('span', attrs={'class': 'timestamp'})]
            tags = tags.findAll({'p' : True, 'span' : True})
            text = u''.join(unicode(tag) for tag in tags)
            return text
        
        def get_main_image(soup):
            try:
                soup = soup.find('div', attrs={'class' : 'story-body'})
                image_src = soup.img['src']
                _, name = tempfile.mkstemp()
                urllib.urlretrieve(image_src, name)
                extract_tag(soup.img)
                return name
            except Exception as e:
                print "Error: can't download image, error {}".format(e)
        
        def get_other_images(soup):
            try:
                soup = soup.find('div', attrs={'class' : 'story-body'})
                imgs = soup.findAll('img')
                images = []
                for img in imgs:
                    image_src = img['src']
                    _, name = tempfile.mkstemp()
                    urllib.urlretrieve(image_src, name)
                    images.append(name)
                return images
            except Exception as e:
                print "Error: can't download image, error {}".format(e)
        
        soup = BeautifulSoup(urllib.urlopen(link["link"]))
        return {
             "title": link["title"],
             "preview": soup.find('p', attrs={'class': 'introduction'}).string,
             "image": get_main_image(soup),
             "gallery": get_other_images(soup),
             "text": get_text(soup)
             }
    
    def insert_to_db(self, category, a):
        category = Category.objects.get(name=category)
        art = Article(
                              title=a["title"],
                              preview=a["preview"], 
                              text=a["text"], 
                              category=category,
                              publication_date=datetime.datetime.now(),
                              slug= re.subn("[^a-zA-Z0-9]", '', a["title"])[0], 
                              is_important=False,
                              visibility=0,
                             )
        art.save()
        with open(a['image'], 'r') as f:
                    myfile = File(f)
                    img = Picture(
                                  name=art.title,
                                  image=myfile,
                                  is_main_pic=True,
                                  article=art,
                                  )
                    img.save()
        for img in a['gallery']:
            f = open(img, 'r')
            myfile = File(f)
            img = Picture(
                          name=art.title,
                          image=myfile,
                          is_main_pic=False,
                          article=art,
                          )
            img.save()
            f.close()

    def parse(self, category, link):
        links = self.parse_rss(category, link)
        for link in links:
            try:
                a = self.parse_html(link)
                print "OK: link %s downloaded" % link['link']
                self.insert_to_db(category, a)
                print "OK: link %s inserted to DB" % link['link']
            except Exception as e:
                print e
                