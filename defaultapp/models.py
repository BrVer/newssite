from unicodedata import category
from django.db import models
from mptt.models import MPTTModel, TreeForeignKey
from sorl.thumbnail import ImageField
from sorl.thumbnail.shortcuts import get_thumbnail
import mptt
import datetime
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic
from django.db.models.signals import post_save

class Tag(models.Model):
    name = models.CharField(max_length=30)
    def __unicode__(self):
        return self.name

class Metatag(models.Model):
    description = models.CharField(max_length=155)
    keywords = models.TextField()
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = generic.GenericForeignKey('content_type', 'object_id')

    def __unicode__(self):
        return self.description

class PublishedManager(models.Manager):
    def get_query_set(self):
        return super(PublishedManager, self).get_query_set().filter(publication_date__lte = datetime.datetime.now())
    def get_important(self):
        return self.get_query_set().filter(is_important=True).order_by('-publication_date')[:6]
    def get_last_week(self):
        return self.get_query_set().filter(publication_date__gt=(datetime.datetime.now() - datetime.timedelta(weeks=1)))

class Article(models.Model):
    category = models.ForeignKey('Category')
    slug = models.SlugField()
    title = models.CharField(max_length=300)
    preview = models.TextField(max_length=3000)
    text = models.TextField()
    publication_date = models.DateTimeField()
    visits_count = models.PositiveIntegerField(blank=True, null=True, default=0)
    is_important = models.BooleanField()
    tags = models.ManyToManyField(Tag, blank=True, null=True)
    related_articles = models.ManyToManyField('Article', blank=True, null=True)

    objects = models.Manager()
    published = PublishedManager()


    VISIBLE_TO_ALL = 0
    VISIBLE_TO_AUTHORIZED = 1

    visibility = models.PositiveSmallIntegerField(
        choices=[(VISIBLE_TO_ALL, "visible to all users"),
                 (VISIBLE_TO_AUTHORIZED, "visible only to authorized users")])
    def get_main_image(self):
        img = self.picture_set.filter(is_main_pic=True)[:1]
        return img[0] if img else None
    
    def get_absolute_url(self):
        return '/article/%s/' % self.slug
    
    def __unicode__(self):
        return u'%s (category: %s)' % (self.title, self.category)


class Category(MPTTModel):
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children')
    name = models.CharField(max_length=50, unique=True)
    is_active = models.BooleanField()
    slug = models.SlugField()
    tags = generic.GenericRelation(Metatag)

    def __unicode__(self):
        return self.name
    def get_last_5_news(self):
        news = Article.objects.filter(category = self.id)
        descendants = self.get_descendants()
        for child in descendants:
            news = news|Article.objects.filter(category = child.id)
        return news.order_by("-publication_date")[:5]
    class Meta:
        verbose_name_plural = "Categories"
        ordering = ['tree_id', 'lft']
    class MPTTMeta:
        order_insertion_by = ['name']

mptt.register(Category,)


class Picture(models.Model):
    name = models.CharField(max_length=100)
    image = ImageField(upload_to='images')
    is_main_pic = models.NullBooleanField()
    article = models.ForeignKey(Article)

    def get_thumbnail_html(self):
        img = self.image
        img_resize_url = unicode(get_thumbnail(img, '100x100').url)
        html = '<a class="image-picker" href="%s"><img src="%s" alt="%s"/></a>'
        return html % (self.image.url, img_resize_url, self.name)
    get_thumbnail_html.short_description = u'Mini'
    get_thumbnail_html.allow_tags = True





#def create_metatag(sender, instance, created, **kwargs):
#    if created:
#        type = ContentType.objects.get_for_model(instance.__class__)
#        t = Metatag(content_object=instance)
#        list = []
#        if(type == ContentType.objects.get_for_model(Article)):
#            list+=instance.category.get_ancestors(True, True).values_list('name', flat = True)
#            list+=(instance.tags.values_list('name',flat = True))
#            t.description = instance.preview[:150]
#        if(type == ContentType.objects.get_for_model(Category)):
#            list+=instance.get_ancestors(True, True).values('title')
#        t.keywords = ', '.join(list)
#        t.save()
#
#post_save.connect(create_metatag, sender=Article)
#post_save.connect(create_metatag, sender=Category)
