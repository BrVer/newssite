from django import template

register = template.Library()

@register.inclusion_tag("defaultapp/paginator.html")
def paginator(collection):
    return {'page': collection}

@register.inclusion_tag("defaultapp/image.html")
def image(image, size=None, link=None):
    s = size.split('x')
    return {'image': image, 'size': size, 'link': link, 'width': s[0], 'height': s[1]}

@register.simple_tag(takes_context=True)
def active_if_section(context, section):
    if context.get('current_section') == section:
        return "active"
    else:
        return ""
