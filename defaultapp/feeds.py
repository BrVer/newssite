from django.contrib.syndication.views import Feed
from defaultapp.models import Article

class LatestArticles(Feed):
    title = "vzpm.org site news"
    link = "/sitenews/"
    description = "Updates on changes and additions to vzpm.org."

    def items(self):
        return Article.objects.order_by('-publication_date')[:5]
    
    def item_title(self, item):
        return item.title

    def item_description(self, item):
        return item.preview
    
    def item_link(self, item):
        return item.get_absolute_url()
