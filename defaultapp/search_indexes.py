import haystack.indexes as indexes
from defaultapp.models import Article
import datetime
from haystack import site

class ArticleIndex(indexes.RealTimeSearchIndex):

    text = indexes.CharField(use_template=True, document=True)
    title = indexes.CharField(model_attr='title')
    preview = indexes.CharField(model_attr='preview')
    publication_date = indexes.DateTimeField(model_attr='publication_date')

    def index_queryset(self):
        return Article.published.all()


site.register(Article, ArticleIndex)
