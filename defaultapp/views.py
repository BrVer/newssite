from django.shortcuts import get_object_or_404, render
from defaultapp.models import Article, Category, Metatag, Tag
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.contenttypes.models import ContentType
#from django.views.decorators.cache import cache_page

import datetime

from haystack.forms import SearchForm
from haystack.query import SearchQuerySet
from haystack.views import SearchView

from django.db.models import F

#@cache_page(60*3)
def home(request):
    categories = Category.tree.root_nodes()
    important = Article.published.get_important()
    return render(request, 'main.html', {'cats': categories, 'important': important })

def show_article(request, sl):
    article = get_object_or_404(Article.published.select_related(), slug=sl)
    Article.published.filter(pk=article.pk).update(visits_count=(F('visits_count') + 1))
    related = article.related_articles.all()
    tags = article.tags.all()
    similar =  Article.published.filter(tags__in=tags).distinct().exclude(id=article.id).exclude(id__in=related.values('id')).order_by("-publication_date")[:10]
    breadcrumb = article.category.get_ancestors(include_self=True)
    gallery = article.picture_set.all()
    comments = article.comments.all()
    metatags, created = Metatag.objects.get_or_create(content_type_id=ContentType.objects.get_for_model(Article).id, object_id=article.id)
    if created:
        list = ['news']
        list+=article.category.get_ancestors(True, True).values_list('name', flat = True)
        list+=article.tags.values_list('name', flat = True)
        metatags.description = article.preview[:150]
        metatags.keywords = ', '.join(set(list))
        metatags.save()
    return render(request, 'article.html', locals())

def show_category(request, cat_slug):
    cat = get_object_or_404(Category, slug = cat_slug)
    articles = Article.published.filter(category = cat).order_by('-publication_date')[:20]
    breadcrumb = cat.get_ancestors(include_self=True)
    metatags, created = Metatag.objects.get_or_create(content_type_id=ContentType.objects.get_for_model(Category).id, object_id=cat.id)
    if created:
        list = ['news']
        list+=cat.get_ancestors(True, True).values_list('name', flat = True)
        metatags.keywords = ', '.join(list)
        metatags.save()
    return render(request, 'category.html', locals())

def show_articles_list(request, articles_list, template_name, page_size):
    paginator = Paginator(articles_list, page_size)
    page = request.GET.get('page')
    try:
        articles = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        articles = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        articles = paginator.page(paginator.num_pages)
    return render(request, template_name, {"articles": articles})


def show_latest(request):
    articles_list = Article.published.order_by('-publication_date').\
                values('title','slug','category__name','publication_date', 'preview')
    return show_articles_list(request, articles_list, 'by_date.html', 15)

def show_popular(request):
    articles_list = Article.published.get_last_week().order_by('-visits_count')
    return show_articles_list(request, articles_list, 'by_popularity.html', 10)

def tag(request, tag):
    tag = get_object_or_404(Tag, name = tag)
    articles_list =  tag.article_set.order_by('-publication_date')
    return show_articles_list(request, articles_list, 'tag.html', 15)

search_article = SearchView(form_class=SearchForm, searchqueryset=SearchQuerySet().models(Article))

