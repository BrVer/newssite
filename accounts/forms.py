from django import forms
import django
from accounts.models import *
from django.forms.extras.widgets import SelectDateWidget
import datetime

class ProfileForm(django.forms.ModelForm):
    first_name = django.forms.CharField(max_length=30, required=False)
    last_name = django.forms.CharField(max_length=30, required=False)
    email = django.forms.EmailField(max_length=30, required=True)

    def __init__(self, *args, **kwargs):
        self.prof = kwargs.get('instance', None)
        initial = {'first_name': self.prof.user.first_name, 'last_name': self.prof.user.last_name, 'email': self.prof.user.email}
        kwargs['initial'] = initial
        super(ProfileForm, self).__init__(*args, **kwargs)

    class Meta:
        model = UserProfile
        exclude = ('user',)
        widgets = {
            'date_of_birth': SelectDateWidget(years= range(1930, datetime.datetime.now().year+1)),
            }

    def save(self, commit=True):
        super(ProfileForm, self).save(commit)
        if commit:
            self.prof.user.first_name = self.cleaned_data['first_name']
            self.prof.user.last_name = self.cleaned_data['last_name']
            self.prof.user.email = self.cleaned_data['email']
            self.prof.user.save()

